package ar.com.besy.microservices.futbolmanagercore.repositories;

import ar.com.besy.microservices.futbolmanagercore.entities.TeamEntity;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

@DataJpaTest
public class TeamRepositoryTest {

	@Autowired
	private TeamRepository teamRepostory;

	@Test
	public void WhenFindById_thenReturnTeam() {

		TeamEntity teamEntity = new TeamEntity(1, "Arsenal");
		teamRepostory.save(teamEntity);

		// AssertJ
		Assertions.assertThat(teamRepostory.findById(1))
										.isNotEmpty()
										.hasValue(teamEntity);
	}

}
