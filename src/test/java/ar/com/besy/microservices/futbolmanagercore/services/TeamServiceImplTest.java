package ar.com.besy.microservices.futbolmanagercore.services;

import ar.com.besy.microservices.futbolmanagercore.entities.TeamEntity;
import ar.com.besy.microservices.futbolmanagercore.mappers.TeamMapper;
import ar.com.besy.microservices.futbolmanagercore.model.TeamDTO;
import ar.com.besy.microservices.futbolmanagercore.repositories.TeamRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.condition.EnabledOnOs;
import org.junit.jupiter.api.condition.OS;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import uk.co.jemos.podam.api.PodamFactory;
import uk.co.jemos.podam.api.PodamFactoryImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.verify;

@Slf4j
public class TeamServiceImplTest {

	@InjectMocks
	private TeamArgServiceImpl teamServiceImpl;

	@Mock
	private TeamRepository teamRepository;
	
	@Mock
	private TeamMapper teamMapper;
	
	private static List<TeamEntity> teamEntities;
	
	private static List<TeamDTO> teamDtos;

	private static PodamFactory factory;


	@BeforeAll
	static void setup() {

		factory = new PodamFactoryImpl();
		factory.getStrategy().setMemoization(false);

		teamEntities = new ArrayList<>();
		teamDtos = new ArrayList<>();

		for (int i = 0; i < 100; i++) {
			TeamEntity teamEntity = factory.manufacturePojoWithFullData(TeamEntity.class);
			teamEntities.add(teamEntity);

			//log.debug("Team entity {}", teamEntity);
			log.info("Team entity {}",teamEntity);


		}

		for (int i = 0; i < 100; i++) {

			TeamDTO teamDTO = factory.manufacturePojoWithFullData(TeamDTO.class);
			teamDtos.add(teamDTO);
		}

		 teamDtos = List.of(new TeamDTO(1, "River"),new TeamDTO(2, "Boca"),new TeamDTO(3, "Huracan"));
	}

	@BeforeEach
	public void setUpBefore() {

		log.info("@BeforeAll - executes once before all test methods in this class");

		MockitoAnnotations.initMocks(this);
		
		//teamServiceImpl = new TeamArgServiceImpl(teamRepository, teamMapper);

		log.info("Build Mock for teamRepository");
		Mockito.when(teamRepository.findAll()).thenReturn(teamEntities);
		Mockito.when(teamMapper.getTeamsDtos(teamEntities)).thenReturn(teamDtos);
		
		log.info("@BeforeEach - executed before each test method");

	}

	@Test
	@DisplayName("Test get team list")
	@Order(1)
	//@Disabled
	@EnabledOnOs(OS.WINDOWS)
	public void test_when_listTeam_then_returnAllTeams() {
		
		log.info("Get all teams");

		//Efective Test method
		List<TeamDTO> listAllTeams = teamServiceImpl.findAllTeams();
		
		//Mockito verify
		verify(teamRepository,atLeast(1)).findAll();
		verify(teamMapper,atLeast(1)).getTeamsDtos(teamEntities);
		
		//Asserts
		assertTrue(listAllTeams.size()>2);
		Optional<TeamDTO> first = listAllTeams.stream().findFirst();
		assertEquals(first.get().getTitle(),"Equipo River");
	}

	@ParameterizedTest
	@ValueSource(ints = { 1, 2, 3 })
	public final void sum(int i) {
		log.info("valor i " + i);
		assertTrue(i > 0);
	}



	@AfterEach
	void tearDown() {
		log.info("@AfterEach - executed after each test method.");
	}

	@AfterAll
	static void done() {
		log.info("@AfterAll - executed after all test methods.");

	}

}
