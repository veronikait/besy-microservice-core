package ar.com.besy.microservices.futbolmanagercore.controllers;

import ar.com.besy.microservices.futbolmanagercore.configurations.AppConfiguration;
import ar.com.besy.microservices.futbolmanagercore.configurations.HatoeasConfiguration;
import ar.com.besy.microservices.futbolmanagercore.controllers.TeamControllerRest;
import ar.com.besy.microservices.futbolmanagercore.controllers.helpers.HatoeasTeamHelper;
import ar.com.besy.microservices.futbolmanagercore.model.TeamDTO;
import ar.com.besy.microservices.futbolmanagercore.services.TeamArgServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@DisplayName("Test teamController : Controller Layer")
@WebMvcTest(TeamControllerRest.class)
@Import(HatoeasConfiguration.class)
@Slf4j
public class TeamRestControllerIntegrationTest {

	@MockBean
	private TeamArgServiceImpl teamServiceImpl;

	@MockBean
	private HatoeasTeamHelper hatoeasTeamHelper;

	@MockBean
	private AppConfiguration configurationApp;

	@Autowired
	private MockMvc mvc;

	@BeforeEach
	public void setUpBefore() {

		log.info("Init test");

		MockitoAnnotations.initMocks(this);

		List<TeamDTO> teams = List.of(new TeamDTO(1, "River"), new TeamDTO(2, "Boca"), new TeamDTO(3, "Huracan"));
		Mockito.when(teamServiceImpl.findAllTeams()).thenReturn(teams);

		CollectionModel<TeamDTO> teamsCollectionModel = CollectionModel.of(teams);
		Mockito.when(hatoeasTeamHelper.generateLinksSelfList(teams)).thenReturn(teamsCollectionModel);

		log.info("call list all");
		List<TeamDTO> listAll = teamServiceImpl.findAllTeams();
		listAll.forEach(t -> log.info("team  " + t));
	}

	@Test
	@DisplayName("Test get all teams from rest controller")
	public void givenTeams_whenGetTeams_thenReturnJsonArrayWithTeams() throws Exception {

		// JsonPath y Hamcrest
		mvc.perform(get("/teams/")
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.content").isArray())
				.andExpect(jsonPath("$.content", hasSize(3)))
				.andExpect(jsonPath("$.content[0].name", is("River")))
				.andExpect(jsonPath("$.content[1].name", is("Boca")))
				.andExpect(jsonPath("$.content[2].id", is(3)));

	}

}
