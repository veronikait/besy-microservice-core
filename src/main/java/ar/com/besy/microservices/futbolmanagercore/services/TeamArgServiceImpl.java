package ar.com.besy.microservices.futbolmanagercore.services;

import ar.com.besy.microservices.futbolmanagercore.entities.TeamEntity;
import ar.com.besy.microservices.futbolmanagercore.mappers.TeamMapper;
import ar.com.besy.microservices.futbolmanagercore.model.TeamDTO;
import ar.com.besy.microservices.futbolmanagercore.repositories.TeamRepository;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Lazy
@Qualifier("arg")
@ConditionalOnProperty(prefix = "app", name = "edition", havingValue = "argentina")
@RequiredArgsConstructor
@Service
public class TeamArgServiceImpl implements TeamService {

    @NonNull
    private TeamRepository teamRepository;

    @NonNull
    private TeamMapper teamMapper;


    public Optional<TeamDTO> getTeamById(Integer id) {

        Optional<TeamEntity> optionTeamEntity = teamRepository.findById(id);
        TeamDTO teamDTO = teamMapper.getTeamDto(optionTeamEntity.get());

        return Optional.ofNullable(teamDTO);// optionalTeam;

    }

    public List<TeamDTO> findAllTeams(Pageable pageable) {

        Page<TeamEntity> pageTeams = teamRepository.findAll(pageable);
        List<TeamEntity> teamsEntities = pageTeams.getContent();
        List<TeamDTO> teamsDtos = teamMapper.getTeamsDtos(teamsEntities);

        return teamsDtos;

    }

    public List<TeamDTO> findAllTeams() {

        List<TeamEntity> teamsEntities = teamRepository.findAll();
        List<TeamDTO> teamsDtos = teamMapper.getTeamsDtos(teamsEntities);

        //Logica de negocio  80
        teamsDtos.forEach(teamDTO -> teamDTO.setTitle("Equipo " + teamDTO.getName()));

        return teamsDtos;

    }

    public Integer saveTeam(TeamDTO teamDTO) {

        TeamEntity teamEntity = teamMapper.getTeamEntity(teamDTO);
        teamEntity = teamRepository.save(teamEntity);

        return teamEntity.getId();

    }

    public void deleteById(Integer id) {

        teamRepository.deleteById(id);

    }


}
