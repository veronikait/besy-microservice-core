package ar.com.besy.microservices.futbolmanagercore.entities;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.time.LocalDate;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
@Entity(name = "equipos")
public class TeamEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @NonNull
    private Integer id;

    @NonNull
    @Column(name = "nombre_equipo")
    private String name;

    private int year;

    private String logo;

    private boolean active;

    private LocalDate fundationDate;

    private String mail;

    private String cuit;

    private String title;

    private String body;


}
