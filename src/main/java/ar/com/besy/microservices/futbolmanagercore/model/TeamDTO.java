package ar.com.besy.microservices.futbolmanagercore.model;

import ar.com.besy.microservices.futbolmanagercore.validators.CUIT;
import ar.com.besy.microservices.futbolmanagercore.validators.OnCreate;
import ar.com.besy.microservices.futbolmanagercore.validators.OnUpdate;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.springframework.hateoas.RepresentationModel;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.time.LocalDate;

//ctrl + f2
@Getter
@Setter
@NoArgsConstructor
        //(staticName = "of")
@AllArgsConstructor
@RequiredArgsConstructor
@EqualsAndHashCode
@ToString
@ApiModel(description = "Futbol Team")
public class TeamDTO extends RepresentationModel<TeamDTO> {

    @ApiModelProperty(notes = "Unique identifier of the team.", example = "1", required = true, position = 0)
    @NonNull
    @NotNull
    private Integer id;

    @NonNull
    @NotBlank
    private String name;

    //@Min(1700)
   //@Max(2000)
    @PositiveOrZero(message = "{app.field.validation.error.year}")
    @ToString.Exclude
    private int year;

    @Size(min = 4,max = 20)
    private String logo;

    @AssertTrue(groups = OnUpdate.class)
    @AssertFalse(groups = OnCreate.class)
    private boolean active;

    @Past
    private LocalDate fundationDate;

    @Email
    private String mail;

    @CUIT
    private String cuit;

    private String title;

    private String body;

}
