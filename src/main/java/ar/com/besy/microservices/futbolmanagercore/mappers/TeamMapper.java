package ar.com.besy.microservices.futbolmanagercore.mappers;

import ar.com.besy.microservices.futbolmanagercore.entities.TeamEntity;
import ar.com.besy.microservices.futbolmanagercore.model.TeamDTO;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TeamMapper {

    public TeamDTO getTeamDto(TeamEntity teamEntity);

    public TeamEntity getTeamEntity(TeamDTO teamDTO);

    public List<TeamDTO> getTeamsDtos(List<TeamEntity> teamsEntity);

    public List<TeamEntity> getTeamsEntities(List<TeamDTO> teamDtos);

}

