package ar.com.besy.microservices.futbolmanagercore.controllers.helpers;

import ar.com.besy.microservices.futbolmanagercore.controllers.TeamControllerRest;
import ar.com.besy.microservices.futbolmanagercore.model.TeamDTO;
import lombok.Data;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.Link;
import org.springframework.stereotype.Component;

import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;


@Component
@Data
public class HatoeasTeamHelper {

    public  void generateSelfLink(TeamDTO teamDTO) throws Exception {

        Link withSelfRel = linkTo(methodOn(TeamControllerRest.class).getTeamById(teamDTO.getId())).withSelfRel();
        teamDTO.add(withSelfRel);

    }

    public  CollectionModel<TeamDTO> generateLinksSelfList(List<TeamDTO> teams, Pageable pageable) {

        Link link = linkTo(methodOn(TeamControllerRest.class).getAllTeams(pageable)).withSelfRel();
        CollectionModel<TeamDTO> result = CollectionModel.of(teams, link);
        return result;
    }

    public  CollectionModel<TeamDTO> generateLinksSelfList(List<TeamDTO> teams) {

        Link link = linkTo(methodOn(TeamControllerRest.class).getAllTeams()).withSelfRel();
        CollectionModel<TeamDTO> result = CollectionModel.of(teams, link);
        return result;
    }

    public  void generatePlayersLink(TeamDTO teamDTO) {

        //Creo un link
        Link accountsRel = linkTo(methodOn(TeamControllerRest.class).getTeamPlayers(teamDTO.getId())).withRel("players");
        teamDTO.add(accountsRel);

    }


}
