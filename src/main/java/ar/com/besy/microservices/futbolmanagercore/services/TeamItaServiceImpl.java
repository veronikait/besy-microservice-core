package ar.com.besy.microservices.futbolmanagercore.services;

import ar.com.besy.microservices.futbolmanagercore.client.TeamClient;
import ar.com.besy.microservices.futbolmanagercore.model.TeamDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

//@Primary
@Lazy
@Qualifier("ita")
@ConditionalOnProperty(prefix = "app",name = "edition",havingValue = "italia")
@Service
public class TeamItaServiceImpl implements TeamService {

    @Autowired
    private TeamClient teamClient;

    public Optional<TeamDTO> getTeamById(Integer id){

        TeamDTO teamDTO = teamClient.getTeamById(id);

        return Optional.ofNullable(teamDTO);

    }

    public List<TeamDTO> findAllTeams(){
        return null;
    }

    @Override
    public List<TeamDTO> findAllTeams(Pageable pageable) {
        return null;
    }



    @Override
    public Integer saveTeam(TeamDTO teamDTO) {
        return null;
    }

    @Override
    public void deleteById(Integer id) {

    }

}
