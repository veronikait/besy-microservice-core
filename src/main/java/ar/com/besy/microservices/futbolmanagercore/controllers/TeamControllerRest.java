package ar.com.besy.microservices.futbolmanagercore.controllers;

import ar.com.besy.microservices.futbolmanagercore.configurations.AppConfiguration;
import ar.com.besy.microservices.futbolmanagercore.controllers.helpers.HatoeasTeamHelper;
import ar.com.besy.microservices.futbolmanagercore.model.PlayerDTO;
import ar.com.besy.microservices.futbolmanagercore.model.TeamDTO;
import ar.com.besy.microservices.futbolmanagercore.services.TeamService;
import ar.com.besy.microservices.futbolmanagercore.validators.OnCreate;
import ar.com.besy.microservices.futbolmanagercore.validators.OnUpdate;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.MalformedURLException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;


@Slf4j
@RestController
@RequestMapping("/teams")
@Api(tags = "Futbol Team API")
public class TeamControllerRest {

    //private final Logger logger = LoggerFactory.getLogger(TeamControllerRest.class);

    @Autowired
    private AppConfiguration appConfiguration;

    @Autowired
    private HatoeasTeamHelper hatoeasTeamHelper;

    //@Qualifier("arg")
    @Autowired
    private TeamService teamService;

    @GetMapping("/saludo/{userId}")
    public String holaMundo(@PathVariable String userId) {

        MDC.put("userid",userId);

        log.trace("Ejecutando hola mundo trace");
        log.debug("Ejecutando hola mundo debug");
        log.info("Ejecutando hola mundo info");
        log.warn("Ejecutando hola mundo warn");
        log.error("Ejecutando hola mundo error");

        return "Hola Mundo Spring Rest " + appConfiguration.toString();
    }

    @ApiOperation(notes="Retrieve one team by id",value="Get team by id")
    @GetMapping("/{id}")
    public ResponseEntity<TeamDTO> getTeamById(
            @ApiParam(example = "1",value = "Identifier for Team",allowableValues = "1,2,3,4",required = true)
            @PathVariable Integer id) throws Exception {

        Optional<TeamDTO> teamDTOOptional = teamService.getTeamById(id);
        TeamDTO teamDTO = null;

        try {

            teamDTO = teamDTOOptional.orElseThrow(NoSuchElementException::new);
            hatoeasTeamHelper.generateSelfLink(teamDTO);

        }catch (NoSuchElementException e) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(teamDTO);

    }

    @GetMapping("pageable")
    public ResponseEntity<CollectionModel<TeamDTO>> getAllTeams(
                    @PageableDefault(size = 3,sort = {"name","year"},direction = Sort.Direction.ASC)
                    Pageable pageable) {

        List<TeamDTO> teams = teamService.findAllTeams(pageable);

        CollectionModel<TeamDTO> teamsModel =  hatoeasTeamHelper.generateLinksSelfList(teams,pageable);
        teams.forEach(team -> {
            try {
                hatoeasTeamHelper.generateSelfLink(team);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        teams.forEach(team -> hatoeasTeamHelper.generatePlayersLink(team));

        return ResponseEntity.ok(teamsModel);

    }


    @GetMapping
    public ResponseEntity<CollectionModel<TeamDTO>> getAllTeams() {

        List<TeamDTO> teams = teamService.findAllTeams();

        CollectionModel<TeamDTO> teamsModel =  hatoeasTeamHelper.generateLinksSelfList(teams);

        teams.forEach(team -> {
            try {
                hatoeasTeamHelper.generateSelfLink(team);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        teams.forEach(team -> hatoeasTeamHelper.generatePlayersLink(team));

        return ResponseEntity.ok(teamsModel);

    }

    @PostMapping
    public ResponseEntity<String> saveTeam(@Validated(OnCreate.class) @RequestBody TeamDTO teamDTO) throws MalformedURLException {

        System.out.println("Saving team..... " + teamDTO);

        Integer id = teamService.saveTeam(teamDTO);

        URI location = ServletUriComponentsBuilder.
                fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(id)
                .toUri();

        return ResponseEntity.created(location).build();

    }

    @PutMapping
    public ResponseEntity<TeamDTO> updateTeam(@Validated(OnUpdate.class) @RequestBody TeamDTO teamDTO) {

        System.out.println("Saving team..... " + teamDTO);

        if (teamDTO == null){

            ResponseEntity.notFound().build();

        }


        return ResponseEntity.ok(teamDTO);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteTeamById(@PathVariable Integer id) {

        System.out.println("eliminando team con id" + id);

        if (id == 3){

            ResponseEntity.notFound().build();

        }
        teamService.deleteById(id);

        return ResponseEntity.ok().build();
    }


    @GetMapping("/{id}/players/{idPlayer}")
    public ResponseEntity<PlayerDTO> getTeamPlayerById(@PathVariable Integer id, @PathVariable Integer idPlayer) {

        PlayerDTO playerDTO = new PlayerDTO(idPlayer, "Batistuta");

        if (playerDTO == null){

            return ResponseEntity.notFound().build();

        }

        return ResponseEntity.ok(playerDTO);
    }

    @GetMapping("/{id}/players")
    public ResponseEntity<List<PlayerDTO>> getTeamPlayers(@PathVariable Integer id) {

        List<PlayerDTO> players = new ArrayList<>();
        players.add(new PlayerDTO(1, "Batistuta"));
        players.add(new PlayerDTO(2, "Balbo"));
        players.add(new PlayerDTO(3, "Vazquez"));

        return ResponseEntity.ok(players);
    }

}
