package ar.com.besy.microservices.futbolmanagercore.model;

import lombok.Data;
import lombok.NonNull;

@Data
public class PlayerDTO {

    @NonNull
    private Integer id;

    @NonNull
    private String name;

    private String lastName;

    private Integer number;

    private Integer gol;
}
